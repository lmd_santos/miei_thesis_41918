%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter4.tex
%% NOVA thesis document file
%%
%% Chapter with lots of dummy text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\chapter{Processing}
\label{cha:processing}


\glspl{cpu} are optimized for low-latency access to cached datasets and control logic for unordered and speculative execution. Whereas \glspl{gpu} are more suited for data-parallel and throughput computations with a latency-tolerant architecture with more transistors dedicated to computation \cite{intro_gpu_comp}. This brings us to the drastic increase of the transistor density in recent years, which equates to a larger amount of thousands of millions of floating point operations per second (GFLOPS) on \glspl{gpu} in comparison to \glspl{cpu}.

\begin{minipage}{0.95\textwidth}
	\centering
	\includegraphics[width=0.8\textwidth]{cpu_gpu_gflops}
	\captionof{figure}{CPU vs GPU GFLOPS comparison over time}
	\label{fig:cpu_gpu_gflops}
	\small source: \url{https://videocardz.com/nvidia} and \url{ https://asteroidsathome.net/boinc/cpu_list.php}

\end{minipage}

NVIDIA's GPUs are highly parallel devices which have thousands of threads running concurrently at a given time on their cores. Thus, because of their immense computational power they are much faster than \glspl{cpu}.

\paragraph{CUDA}
Compute Unified Device Architecture, is a parallel programming paradigm released in 2007 by NVIDIA. It is used in the development of software and a variety of applications for GPUs that are highly parallel in their nature and run on hundreds of \gls{gpu} cores.
CUDA has some specific functions, called kernels. A kernel can be a function or a full program invoked by the \gls{cpu}. It is executed N number of times in parallel on \gls{gpu} by using N number of threads. CUDA also has built-in shared memory and synchronization among threads.
CUDA is supported only on NVIDIA’s \glspl{gpu} starting from the Tesla architecture\cite{Inam1994}.


\section{CPU + GPU Processing}


Due to the architectural differences between CPU and \gls{gpu} , they excel in different tasks. \citeauthor{Ghorpade12} \cite{Ghorpade12} compared in his article the different strengths of each hardware component. \gls{cpu} possess really fast caches, and is able to implement a fine branching granularity in addition to being able to manage a diverse assortment of processes and threads. The \gls{cpu} display a high single thread execution performance which is great for task parallelism equating to elevated performance results when executing sequential codes. On the other hand, \glspl{gpu} are composed of many mathematical units with fast access to onboard memory. \gls{gpu} programs run in fragments called kernels which provide high throughput in tasks with data parallelism, specially when it is of an arithmetic nature.

By executing the sequential parts of th program on the CPU and using the \gls{gpu} to accelerate the data intensive part by parallelizing the data-intensive portions on many cores, the program will execute faster due to the use of the CPU on more critical tasks. 



\section{Python + CUDA}
 The major factor for the choice of a high-level, dynamic language instead of a potentially better-performing low-level static one is the complementarity of the \gls{gpu} and the \gls{cpu}. The \gls{gpu} is optimized to execute throughput-oriented parts of programs. This frees the \gls{cpu} to be only responsible for control and communication. This enables Python to perform this job equally well or even better than a low-level language, simply because the performance demands are reduced. As an added benefit, a high-level Python-based compute code requires much less effort from the programmer than a low-level C-based \gls{gpu} compute code. This reduction in effort derives from the data types, resources and abstractions a high-level programming language brings. Entities such as code modules and compute devices are reflected in Python using object-oriented terms this provides a better abstraction than in a low-level C interface. In Python the errors are detected and reported automatically enabling feedback to be given to the programmers \cite{Kloeckner12}.

\section{Python Standard Library}
The standard library for machine learning applications in Python is Scikit-learn, sometimes referred to as Sklearn for short. This library incorporates numerous algorithms that serve a wide range of applications, namely Classification and Clustering amongst others.\\
The classifiers chosen from this library were:
\begin{itemize}
	\item Multilayer Perceptron Classifier;
	\item Gradient Boosting Classifier;
	\item Support Vector Classifier;
	\item K Nearest Neighbour Classifier.
\end{itemize}


\section{Python GPU Accelerated Machine Learning Libraries}
\label{sec:py_lib}
In this section we intend to provide insights on the libraries supported in Python.

\subsection{Library search and criteria}
In order to find the best candidate to serve as the standard classifier counterpart numerous libraries were explored. The criteria used for the library choice was:

\begin{itemize}
	\item Community suggestion;
	\item Library support from the developers;
	\item Library documentation and ease of use.	
\end{itemize}

The libraries taken into account were Tensorflow and Keras for Artificial Neural Networks; CudaTree and XGBoost for ensembles; liquidSVM and pyKMLib for Support Vector Machines; finally knn\textunderscore cuda and community implementations in Tensorflow for K Nearest Neighbours.
Between Keras as Tensorflow, both posed viable choices having the possibility for low level user implementations of the algorithm, but Tensorflow proved the most user-friendly option due to having a "canned" estimators, these are already implemented and provide a plug-and-play experience in addition to its frequently updated documentation and active community.
In the matter of ensembles the CUDATree estimator looked like an interesting option since it displayed an interface compatible with the Scikit-learn library and the author claimed his implementation to have good performance, the downside was that the library had been abandoned by the author. In the meanwhile, XGBoost's authors propose an actively developed library that also is compatible with the Scikit-learn library.
For Support Vector Machines, all but the liquidSVM were either a proof of concept only mocked up as a demo or were implemented with not enough abstraction to provide ease of use to the user. liquidSVM provided a simple implementation that had an interface that was intuitive to use and was accompanied by helpful documentation and examples.
The K Nearest Neighbours options all had really low-level implementations with little to none flexibility or ease of use, associated with low performance on simple tasks with small datasets, none of them proved to be an option worth seeking.
In summary, we define the following libraries and their counterparts:

\mbox{}\\
\begin{minipage}{.95\textwidth}
	\centering
	\captionof{table}{Classifier counterpart setup}
	\label{tab:class_counterparts}
	\begin{tabular}{cc}
		CPU-bound      & GPU-accelerated      \\ \hline
		MLP Classifier & DNN Classifier       \\
		GB Classifier  & XGB Classifier       \\
		SV Classifier  & liquidSVM Classifier \\
		KNN Classifier & -                   
	\end{tabular}
\end{minipage}
\mbox{}\\

In this next part we give a little insight into the each of the selected \gls{gpu} libraries.

\subsection{Tensorflow}
TensorFlow is an open source software library for numerical computation using data flow graphs. The flexibility of the architecture allows deployment in one or more \glspl{cpu} or \glspl{gpu}, server or mobile using a single API. Tensorflow was originally developed by researchers and engineers working on the Google Brain Team within Google's Machine Intelligence research organization for the purposes of conducting machine learning and deep neural networks research.

\subsection{XGBoost}
XGBoost is short for “Extreme Gradient Boosting”, where the term “Gradient Boosting” is proposed in the paper Greedy Function Approximation: A Gradient Boosting Machine, by Friedman. It has a plugin that adds the option for GPU accelerated tree construction and prediction algorithms.

\subsection{liquidSVM}
 liquidSVM is an implementation of \glspl{svm} whose key features are: fully integrated hyper-parameter selection, extreme speed on both small and large data sets, full flexibility for experts, and inclusion of a variety of different learning scenarios.


\section{Benchmarking}

The benchmarking process consists on the acquisition of the time a classifier takes to fit the data, classify the testing samples as well as the total time of all this process. Then graph are generated in order to assert trends and patterns.


\section{Conclusion}

The process of parallelizing machine learning algorithms in GPUs is tempting due to the increase in throughput or the decrease in the overall runtime of the program, but with it some issues must be addressed in order for the parallel version to be on par or above the CPU-bound version. 
The task of identification of which parts of the algorithm have the potential for parallel execution and which are confined to sequential execution, these tasks are non-trivial and have a major impact on the algorithms performance. The process of creating a parallel GPU version of state-of-the-art algorithms requires one to address the particularities of the GPU Architecture.

Python is used for the libraries mentioned in Section \ref{sec:py_lib} due to its capability of operating at higher abstraction level than the low-level \gls{cuda} code. This enables the delegation of computation intensive tasks to \gls{gpu}, while Python assumes a scripting role where it is only responsible for the overall coordination of the program. 