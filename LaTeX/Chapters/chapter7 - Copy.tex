			%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter4.tex
%% NOVA thesis document file
%%
%% Chapter with lots of dummy text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Burned Area Mapping}
\label{cha:burned_area_mapping}

\section{Methodology}

The objective of this study is to evaluate the performance of several algorithms by benchmarking python's Scikit-learn library classifiers against their parallel GPU counterparts. For this task we will use images from different satellites. In particular, we will evaluate the performance
of the \gls{mlp}, \gls{gb}, \gls{svm} and \gls{knn} classifiers using Sentinel-2, Landsat 8 and MODIS imagery.
Our work methodology can be divided into a pre-classification phase which is followed by a classification and a complementary studies phase.

\subsection{Pre-Classification Phase}
Where the rasters are downloaded and processed.

\subsection{Classification Phase}
With all images pre-processed the classification process starts by performing a feature selection followed by parameter fine tuning (explained more explicitly in sections \ref{sec:feat_sel} and \ref{sec:fine_tune}) and finally the classification process itself.
In sum, this phase can be decomposed into the following steps:
\begin{enumerate}
	\item Perform feature selection with the full breadth of features available.
	\item Perform parameter fine-tuning for each of Scikit-learn's classifiers.
	\item Train both the standard and \gls{gpu} classifiers with the same optimal parameters.
	\item Perform testing on the final model and gather statistics on the satellite-classifier-training set combination.
\end{enumerate}

\subsection{Complementary Studies Phase}

In this phase the aim is to use a previously trained classifier and use it in a burned area classification task of every day from the 1st of May to the 31st of August.



\section{Feature Selection}
\label{sec:feat_sel}

In order to perform a feature selection, we applied a LASSO regression to the dataset containing all Sentinel-2 pre and postfire scenario band data and indices, as well as indice variation between those sensing dates (deltas).
The Lasso regression was run several times with different values for the weight parameter. Choosing a single weight value and opt for the resulting feature selection would turn our choice biased. A solution found for the problem was to define an heuristic to help us choose the most relevant features, in this case an average. The mean value for each feature across all the different weight values being greater than zero equates to that particular feature having some relevance. The features selected with this procedure were the ones represented on figure \ref{fig:burned_lasso}.
The remaining features had a mean score of zero, meaning that they are not as relevant as the ones with non-zero values. 
We used the Sentinel-2 imagery due to it having the best resolution of the three, which equates to a greater amount of data points to assist in finding the best combination of features for our models.

	\begin{minipage}{0.95\textwidth}
	\centering
	\includegraphics[width=\textwidth]{burned_lasso}
	\captionof{figure}{Graph of the non-zero LASSO score features}
	\label{fig:burned_lasso}	
	\end{minipage}

\section{Parameter Tuning}
\label{sec:fine_tune}

To discover which were the best parameters for the classifiers used in section \ref{sec:class_process} we performed a 5-fold cross-validation over several parameter values. The data used for parameter tuning was from MODIS imagery in order to account for the bottleneck in samples and resolution that is associated to it.  The best parameters resulting from tuning with this data in order to level the field when it takes to classifier comparison, which is the main focus of our work. We only performed parameter tuning on the Scikit-learn classifiers in order to establish a base-line between them and their \gls{gpu}-accelerated counterparts.

\subsection{Multilayer Perceptron Classifier}

For this classifier we tuned the number of neurons in each hidden layer (\texttt{hidden\_layer\_sizes}) from (1,1) to (9,9). The best setting for the hidden-layers was (9,9) with 89,3\% accuracy.
\mbox{}\\\mbox{}\\
\begin{minipage}{0.8\textwidth}
	\centering
	\includegraphics[width=\textwidth]{mlp_cv}
	\captionof{figure}{Multilayer Perceptron \texttt{hidden\_layer\_sizes} during Cross Validation}
	\label{fig:mlp_cv}	
\end{minipage}

\subsection{Gradient Boosting Classifier}

The parameters chosen for tuning were the maximum depth limits the number of nodes in the tree (\texttt{max\_depth}) and the minimum number of samples required to split an internal node(\texttt{min\_samples\_split}). These parameters ranged from 1 to 10 and 2 to 10 respectively. The best combination was \texttt{max\_depth} at 4 and \texttt{min\_samples\_split} at 2 with an accuracy of 89,1\%.

\begin{minipage}{0.75\textwidth}
	\centering
	\includegraphics[width=\textwidth]{gb_cv}
	\captionof{figure}{Gradient Boosting \texttt{max\_depth} and \texttt{min\_samples\_split} during Cross Validation}
	\label{fig:gb_cv}	
\end{minipage}


\subsection{Support Vector Classsifier}

The parameter chosen for tuning was the Gamma coefficient. This parameter was firstly tuned with a wider range of values. After discovering the preliminary best value for Gamma, we shortened the range around it and set a smaller step. The final best value discovered was a Gamma of 0.39 with an accuracy score of 0.89.

\begin{minipage}{0.75\textwidth}
	\centering
	\includegraphics[width=\textwidth]{svm_cv}
	\captionof{figure}{Support Vector Machines Gamma parameter during Cross Validation}
	\label{fig:svm_cv}	
\end{minipage}

\subsection{K Nearest Neighbours Classifier}

For this classifier we decided to only use odd numbers for the Nearest Neighbours parameter (K) in order to prevent ties while classifying. The best value found for K as 5 with an accuracy score of 87,1\%. This process is depicted in figure \ref{fig:knn_cv}.

\begin{minipage}{0.75\textwidth}
	\centering
	\includegraphics[width=\textwidth]{knn_cv}
	\captionof{figure}{K Nearest Neighbours \texttt{n\_neighbors} during Cross Validation}
	\label{fig:knn_cv}	
\end{minipage}


\section{Classification Process}
\label{sec:class_process}

 
The classification process is staged in an incremental fashion, where the percentage of the training set used is gradually increased on each run. The training set percentages used for this experiment were 1, 5, 10, 20 and 30 percent respectively. The data used in this phase is comprised of the pixel values of satellite imagery. This data was split in two, according to the training set percentage assigned for that run. In order to maintain the ratio between both classes, we used a stratified split. The classification process is illustrated in figure \ref{fig:classification_diag}.



	\begin{minipage}{0.95\textwidth}
	\centering
	\includegraphics[width=\textwidth]{classification_diag}
	\captionof{figure}{Diagram of the classification process}
	\label{fig:classification_diag}
	\end{minipage}
\mbox{}\\

\section{Experimentation setting}

For this experiment we intend exhaust all the possible combinations of training set sizes, satellite imagery and classifiers, this procedure resembles a brute-force approach.
This will give us greater insight to the characteristics of each classifier while letting us observe how it reacts with data provided by each satellite.

\begin{description}
	\item[Brute-force] In computer science context, brute-force is a very general way of finding solutions, this method consists of enumerating all possible solution candidates and checking which candidate solution satisfies/better satisfies the problem.
	It is very easy/simple to implement, and will always find a solution if it exists. It has, however, a computational cost proportional to the number of candidate solutions. This reason, is, the obvious main deterrent of it being chosen to solve all kind of problems as most of the times, Time is a very important factor/resource to consider.	
\end{description} 

	\subsection{Hardware specifications}
		The machine used for this experiment had the following configuration:
		\begin{itemize}
			\item Intel Core i7 6700HQ
			\item 16GB DDR4 RAM
			\item NVIDIA GTX950M 4GB
		\end{itemize}
	\subsection{Libraries used}
	The following libraries were the core of our experiment:
	\begin{itemize}
		\item gdal
		\item numpy
		\item sklearn
		\item tensoflow-gpu
		\item xgboost
		\item liquidSVM
	\end{itemize}





\section{Result Discussion}
In this section we proceed to the analysis of the generated data and metrics.
	\subsection{Organization of result discussion}
	\begin{enumerate}
		\item Artificial Neural Networks

		\item Gradient Boosting

		\item Support Vector Machines

		\item K Nearest Neighbours

		\item Comparison

	\end{enumerate}
	
	\subsection{Artificial Neural Networks}
	
	

	
	\subsubsection{MLP}

	
	\paragraph{Execution Time}
	\mbox{}\\
	\begin{minipage}{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{total_time_mlp}
		\captionof{figure}{}
		\label{fig:mlp_time}	
	\end{minipage}
	
	\paragraph{Confusion Matrix}
	\mbox{}\\
	\begin{minipage}{0.95\textwidth}
		\centering
		\captionof{table}{Confusion Matrix from the MLP Classifier using 10\% training data from Sentinel 2 imagery}
		\label{tab:mlp_cm}
		\begin{tabular}{c|cccc}
			\cline{2-3}
			& \multicolumn{2}{c|}{Reference class}                                                  &                                          &                                                   \\ \hline
			\multicolumn{1}{|c|}{Predicted class}       & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
			\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}3979586 & \multicolumn{1}{c|}{137911}                         & \multicolumn{1}{c|}{4117497}             & \multicolumn{1}{c|}{0.97}                         \\
			\multicolumn{1}{|c|}{Burned}          & 83283                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}833023} & \multicolumn{1}{c|}{916306}              & \multicolumn{1}{c|}{0.91}                         \\ \cline{1-3}
			\multicolumn{1}{|c|}{Reference Total} & 4062869                         & 970934                                              & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.96} \\ \cline{1-3}
			\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.86                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.86} \\ \hline
		\end{tabular}
	\end{minipage}
	
	
	\subsubsection{DNN}
	
	\paragraph{Execution Time}
		\mbox{}\\
		\begin{minipage}{0.95\textwidth}
			\centering
			\includegraphics[width=\textwidth]{total_time_dnn}
			\captionof{figure}{}
			\label{fig:dnn_time}	
		\end{minipage}
		
	\paragraph{Confusion Matrix}
	\mbox{}\\
		\begin{minipage}{0.95\textwidth}
			\centering
			\captionof{table}{Confusion Matrix from DNN Classifier using 30\% training data from Sentinel 2 imagery}
			\label{tab:dnn_cm}
			\begin{tabular}{c|cccc}
				\cline{2-3}
				& \multicolumn{2}{c|}{Reference class}                                                  &                                          &                                                   \\ \hline
				\multicolumn{1}{|c|}{Predicted class} & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
				\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}3094174 & \multicolumn{1}{c|}{155551}                         & \multicolumn{1}{c|}{3249725}             & \multicolumn{1}{c|}{0.95}                         \\
				\multicolumn{1}{|c|}{Burned}          & 65835                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}599620} & \multicolumn{1}{c|}{665455}              & \multicolumn{1}{c|}{0.90}                         \\ \cline{1-3}
				\multicolumn{1}{|c|}{Reference Total} & 3160009                         & 755171                                              & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.94} \\ \cline{1-3}
				\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.79                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.82} \\ \hline
			\end{tabular}
		\end{minipage}

	\subsection{Gradient Boosting}
	\label{subsec:gb}
	

	
	
	\subsubsection{Gradient Boosting Classifier}
	
	\paragraph{Execution Time}
	\mbox{}\\
		\begin{minipage}{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{total_time_gb}
		\captionof{figure}{}
		\label{fig:total_time_gb}	
	\end{minipage}
	
	\paragraph{Confusion Matrix}
	\mbox{}\\	
	\begin{minipage}{0.95\textwidth}
		\centering
		\captionof{table}{Confusion Matrix from GB Classifier using 10\% training data from Sentinel 2 imagery}
		\label{my-label}
		\begin{tabular}{c|cccc}
			\cline{2-3}
			& \multicolumn{2}{c}{Reference class}                                                   &                                          &                                                   \\ \hline
			\multicolumn{1}{|c|}{Predicted class} & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
			\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}3974394 & \multicolumn{1}{c|}{133914}                         & \multicolumn{1}{c|}{4108308}             & \multicolumn{1}{c|}{0.97}                         \\
			\multicolumn{1}{|c|}{Burned}          & 88475                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}837020} & \multicolumn{1}{c|}{925495}              & \multicolumn{1}{c|}{0.90}                         \\ \cline{1-3}
			\multicolumn{1}{|c|}{Reference Total} & 4062869                         & 970934                                              & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.96} \\ \cline{1-3}
			\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.86                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.86} \\ \hline
		\end{tabular}
	\end{minipage}

	\subsubsection{XGBoost}
	
	
		\paragraph{Execution Time}
	\mbox{}\\
	\begin{minipage}{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{total_time_xgb}
		\captionof{figure}{}
		\label{fig:total_time_xgb}	
	\end{minipage}

	\paragraph{Confusion Matrix}
	\mbox{}\\
	\begin{minipage}{0.95\textwidth}
		\centering
		\captionof{table}{Confusion Matrix from XGB Classifier using 10\% training data from Sentinel 2 imagery}
		\label{tab:XGB_cm}
		\begin{tabular}{c|cccc}
			\cline{2-3}
			& \multicolumn{2}{c|}{Reference class}                                                  &                                          &                                                   \\ \hline
			\multicolumn{1}{|c|}{Predicted class} & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
			\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}3975301 & \multicolumn{1}{c|}{134450}                         & \multicolumn{1}{c|}{4109751}             & \multicolumn{1}{c|}{0.97}                         \\
			\multicolumn{1}{|c|}{Burned}          & 87568                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}836484} & \multicolumn{1}{c|}{924052}              & \multicolumn{1}{c|}{0.91}                         \\ \cline{1-3}
			\multicolumn{1}{|c|}{Reference Total} & 4062869                         & 970934                                              & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.96} \\ \cline{1-3}
			\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.86                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.86} \\ \hline
		\end{tabular}
	\end{minipage}
	
	\subsection{Support Vector Machines}
	

    
    
		\subsubsection{Support Vector Classifier}
		
		
				\paragraph{Execution Time}
		\mbox{}\\
		\begin{minipage}{0.8\textwidth}
			\centering
			\includegraphics[width=\textwidth]{total_time_svm}
			\captionof{figure}{}
			\label{fig:total_time_svm}	
		\end{minipage}
		
		\paragraph{Confusion Matrix}
		\mbox{}\\
		\begin{minipage}{0.95\textwidth}
		\centering
		\captionof{table}{Confusion Matrix from SVC Classifier using 5\% training data from Sentinel 2 imagery}
		\label{tab:svc_cm}
		\begin{tabular}{c|cccc}
			\cline{2-3}
			& \multicolumn{2}{c|}{Reference class}                                                  &                                          &                                                   \\ \hline
			\multicolumn{1}{|c|}{Predicted class} & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
			\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}4203848 & \multicolumn{1}{c|}{162079}                         & \multicolumn{1}{c|}{4365927}             & \multicolumn{1}{c|}{0.96}                         \\
			\multicolumn{1}{|c|}{Burned}          & 84737                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}862795} & \multicolumn{1}{c|}{947532}              & \multicolumn{1}{c|}{0.91}                         \\ \cline{1-3}
			\multicolumn{1}{|c|}{Reference Total} & 4288585                         & 1024874                                             & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.95} \\ \cline{1-3}
			\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.84                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.85} \\ \hline
		\end{tabular}	
		\end{minipage}
	
		\subsubsection{liquidSVM Classifier}
		
		
		\paragraph{Execution Time}
		\mbox{}\\
		
		\begin{center}
			\begin{minipage}{0.95\textwidth}
				\centering
				\includegraphics[width=\textwidth]{total_time_lsvm}
				\captionof{figure}{}
				\label{fig:total_time_lsvm}	
			\end{minipage}
		\end{center}
		
		
		\paragraph{Confusion Matrix}
		\mbox{}\\
		\begin{minipage}{0.95\textwidth}
		\centering
		\captionof{table}{Confusion Matrix from liquidSVM Classifier using 10\% training data from Sentinel 2 imagery}
		\label{tab:lsvm_cm}
			\begin{tabular}{c|cccc}
				\cline{2-3}
				& \multicolumn{2}{c|}{Reference class}                                                  &                                          &                                                   \\ \hline
				\multicolumn{1}{|c|}{Predicted class}       & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
				\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}3974205 & \multicolumn{1}{c|}{132740}                         & \multicolumn{1}{c|}{4106945}             & \multicolumn{1}{c|}{0.97}                         \\
				\multicolumn{1}{|c|}{Burned}          & 88664                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}838194} & \multicolumn{1}{c|}{926858}              & \multicolumn{1}{c|}{0.90}                         \\ \cline{1-3}
				\multicolumn{1}{|c|}{Reference Total} & 4062869                         & 970934                                              & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.96} \\ \cline{1-3}
				\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.86                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.86} \\ \hline
			\end{tabular}
		\end{minipage}
	\subsection{K Nearest Neighbours}
	
			\paragraph{Execution Time}
	\mbox{}\\
	\begin{minipage}{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{total_time_knn}
		\captionof{figure}{}
		\label{fig:total_time_knn}	
	\end{minipage}
	
		\paragraph{Confusion Matrix}
		\mbox{}\\
		\begin{minipage}{0.95\textwidth}
		\centering
		\captionof{table}{Confusion Matrix from K Nearest Neighbours Classifier using 10\% training data from Sentinel 2 imagery}
		\label{tab:knn_cm}
			\begin{tabular}{c|cccc}
				\cline{2-3}
				& \multicolumn{2}{c|}{Reference class}                                                  &                                          &                                                   \\ \hline
				\multicolumn{1}{|c|}{Predicted class} & Not Burned                      & \multicolumn{1}{c|}{Burned}                         & \multicolumn{1}{c|}{Predicted Total}     & \multicolumn{1}{c|}{Producer Accuracy}            \\ \hline
				\multicolumn{1}{|c|}{Not Burned}      & \cellcolor[HTML]{C0C0C0}3970301 & \multicolumn{1}{c|}{144358}                         & \multicolumn{1}{c|}{4114659}             & \multicolumn{1}{c|}{0.96}                         \\
				\multicolumn{1}{|c|}{Burned}          & 92568                           & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}826576} & \multicolumn{1}{c|}{919144}              & \multicolumn{1}{c|}{0.90}                         \\ \cline{1-3}
				\multicolumn{1}{|c|}{Reference Total} & 4062869                         & 970934                                              & \cellcolor[HTML]{EFEFEF}Overall Accuracy & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.96} \\ \cline{1-3}
				\multicolumn{1}{|c|}{User Accuracy}   & 0.98                            & 0.85                                                & \cellcolor[HTML]{EFEFEF}Overall Kappa    & \multicolumn{1}{c|}{\cellcolor[HTML]{EFEFEF}0.86} \\ \hline
			\end{tabular}
		\end{minipage}

\subsection{Comparison}
\label{subsec:comparison}

Using the results over multiple runs using several combinations of classifiers, data sources and training set percentages, whisker box charts were created to study the behaviour of both the accuracy (figure \ref{fig:overall_accuracy}) and kappa (figure \ref{fig:overall_kappa}) across these combinations. 

\begin{minipage}{0.8\textwidth}
	\centering
	\includegraphics[width=\textwidth]{overall_accuracy}
	\captionof{figure}{Overall accuracy.}
	\label{fig:overall_accuracy}	
\end{minipage}

\begin{minipage}{0.8\textwidth}
	\centering
	\includegraphics[width=\textwidth]{overall_kappa}
	\captionof{figure}{Overall kappa.}
	\label{fig:overall_kappa}	
\end{minipage}
\mbox{}\\ 

Based on the results illustrated in figures \ref{fig:overall_accuracy} and \ref{fig:overall_kappa}, as well as Appendix \ref{cha:exec_times}, we can infer that the best models for Sentinel 2 and MODIS were generated using XGBoost classifier with 10\% and 20\% respectively. For Landsat 8 the best models were generated using the liquidSVM classifier using 30\% training data.\\
Using classifications from these models confusion maps were generated. While analysing these maps, it becomes evident that they present some gaps inside the areas in comparison to their respective ground truths. This may be caused by the model opting for defining some areas in them contained as not being burned, either for being man-made structures such as quarries or buildings (which exist within the study area) or natural rock formations. These confusion maps a represented in figure \ref{fig:cmap_sat}.

\mbox{}\\ 
\begin{minipage}{.95\textwidth}
	\centering
	\subfloat{a)}{{\includegraphics[width=6cm]{cmap_sentinel} }}%
	\qquad
	\subfloat{b)}{{\includegraphics[width=6cm]{cmap_landsat} }}%
	\subfloat{a)}{{\includegraphics[width=6cm]{cmap_sentinel} }}%
	\qquad
	\subfloat{b)}{{\includegraphics[width=6cm]{cmap_legend} }}%
	
	\captionof{figure}{Confusion maps generated from a) Sentinel 2 classification b) Landsat 8 classification c) MODIS classification. d) legend for the colors in the maps.}%
	\label{fig:cmap_sat}%
\end{minipage}

	

	
\section{Complementary studies}
The aim of these studies is to build upon lessons learned from previously in this study and explore different courses to take from this point.

\subsection{Fire season burned area classification}
The purpose of this experiment is to define a burned area identification for all the days in the fire season (May 1\textsuperscript{st} to October 31\textsuperscript{st}). For this intent we defined our "pre-fire scenario" to the day before the official start of the season (April 30\textsuperscript{th}).
The model chosen was a XGBoost classifier trained with 30\% of the Sentinel 2 dataset and the following parameters: \texttt{max\_depth} = 4,\texttt{min\_samples\_split} = 2 and \texttt{n\_estimators} = 100.

This choice for combination of classifier, parameters and satellite was based on section \ref{subsec:comparison}. Seeing that this classifier achieved the best overall scores using MODIS data. The choice of MODIS is mainly based on its daily availability of products.
The metrics obtained from this process were the following:

\mbox{}\\
\begin{minipage}{.95\textwidth}
	\centering
	\captionof{table}{Fire season classification metrics}
	\label{tab:season_metrics}
	\begin{tabular}{ll}
		Operation                 & Time (s) \\ \hline
		Load training data        & 0.06     \\
		Load fire season data     & 340.33   \\
		Total train time          & 376.05   \\
		Total classification time & 4119.27  \\
		Write classifications     & 42.9     \\ \hline
		Total time                & 4539    
	\end{tabular}
\end{minipage}
\mbox{}\\ 

Figure \ref{fig:fire_season_example} represents an example of classification of our study area after all the wildfire activity has ended. There appears to be some noise or miss classifications of rock formations, but this can be derived from rock formations being classified as burned, yet the greater burned areas seem to be majorly classified.

\mbox{}\\ 
\begin{minipage}{.95\textwidth}
		\centering
	\subfloat{a)}{{\includegraphics[width=6cm]{sentinel_27_09} }}%
	\subfloat{b)}{{\includegraphics[width=6cm]{burned_modis_31_10} }}%

	\captionof{figure}{a) shows the study area on the 27\textsuperscript{th} of September according to Sentinel 2 and b) shows  classification result for 31\textsuperscript{st} of October.}%
	\label{fig:fire_season_example}%
\end{minipage}


\subsection{Identification of areas with high risk of fire}
This experiment has the aim of inferring areas with high risk of fire occurrence. The features used were those comprised in table \ref{tab:risk_features}.

\mbox{}\\
\begin{minipage}{0.95\textwidth}
	\centering
	\captionof{table}{List of features used for risk identification}
	\label{tab:risk_features}
	\begin{tabular}{lll}
		\textbf{Multi Spectral Indices} & \textbf{Fire Weather Indices} & \textbf{Land Features}   \\ \hline
		NDVI                            & FFMC                          & DEM                      \\
		SAVI                            & DMC                           & Slope                    \\
		MSI                             & DC                            & Distance to nearest road \\
		MIRBI                           & ISI                           & Vegetation Covertype     \\
		NBR                             & BUI                           &                          \\
		NBR2                            & FWI                           &                          \\
		NBR3                            & DSR                           &                          \\
		NBR4                            &                               &                          \\
		GNBR                            &                               &                         
	\end{tabular}
\end{minipage}

\mbox{}\\

In order to establish a trend of feature changes over the previous days, we decided to include the Multi Spectral Indices and Fire Weather Indices of the day in question and the two previous days. The Land Features are only used once since they do not tend to change under normal circumstances.
To make sure the ground truth only contains relevant areas, all areas that did not correspond to the training date or did not have a specified date of occurrence were removed. After this the shape was clipped, rasterized and set its resolution to the one of MODIS using a minimum criteria resampling function. This function ensures that if the area has a mix of burned and unburned outcomes it is considered the lather, thus ensuring that only "pure" features are related to a burned outcome. Figure \ref{fig:risk_gtruth} illustrates these final steps.

\mbox{}\\
\begin{minipage}{0.95\textwidth}
	\centering
		\subfloat{a)}{{\includegraphics[width=6cm]{burned_10_8_shape} }}%
		\qquad
		\subfloat{b)}{{\includegraphics[width=6cm]{burned_10_8_gtruth} }}%
		\captionof{figure}{a) shows ICNFs burned areas and b) shows the ground truth used.}%
		\label{fig:risk_gtruth}%
\end{minipage}


\subsubsection{Results}
The results were surprising since the classification for the next day (August 11\textsuperscript{th}), using the previous day to train, resulted in the identification of high risk areas to which the date of occurrence is not specified in the yearly burned areas summary provided by the ICNF. Figure \ref{fig:risk_class} illustrates the results obtained.

\mbox{}\\
\begin{minipage}{0.95\textwidth}
	\centering
	\includegraphics[width=.8\textwidth]{risk_class_11_8}
	\captionof{figure}{Fire hazard identification result analysis.}
	\label{fig:risk_class}	
\end{minipage}
\mbox{}\\

Marked in black we have the ground truth for August 10\textsuperscript{th} and in red we have the results for the fire hazard identification for August 11\textsuperscript{th}. After seeing the results we assume that there may be some patterns in the features of those areas which relate to an outcome of burned areas seen in the training phase.